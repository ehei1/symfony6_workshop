<?php

declare(strict_types=1);

namespace App\Model;

class Search
{
    private string $what;

    private string $where;

    private string $category;

    public function getWhat(): string
    {
        return $this->what;
    }

    public function setWhat(string $what): void
    {
        $this->what = $what;
    }

    public function getWhere(): string
    {
        return $this->where;
    }

    public function setWhere(string $where): void
    {
        $this->where = $where;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
