<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\JobAd;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public const CATEGORY = 5;
    public const JOBADS = 60;
    private Generator $faker;

    public function __construct(private UserPasswordHasherInterface $hasher)
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        //<editor-fold desc="Admin">
        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setFullName('Issam KHADIRI');
        $admin->setEmail('khadiri.issam@gmail.com');
        $hashedPassword = $this->hasher->hashPassword(user: $admin, plainPassword: '0000');
        $admin->setPassword($hashedPassword);
        $manager->persist($admin);
        //</editor-fold>

        //<editor-fold desc="Recruitments">
        $agency1 = new User();
        $agency1->setRoles(['ROLE_RECRUITMENT_AGENCY']);
        $agency1->setFullName('RECRUITMENT  AGENCY');
        $agency1->setEmail('recruitment1@gmail.com');
        $hashedPassword = $this->hasher->hashPassword(user: $agency1, plainPassword: '0000');
        $agency1->setPassword($hashedPassword);
        $manager->persist($agency1);
        $this->addReference('MANAGER1', $agency1);

        $agency2 = new User();
        $agency2->setRoles(['ROLE_RECRUITMENT_AGENCY']);
        $agency2->setFullName('RECRUITMENT  AGENCY');
        $agency2->setEmail('recruitment2@gmail.com');
        $hashedPassword = $this->hasher->hashPassword(user: $agency2, plainPassword: '0000');
        $agency2->setPassword($hashedPassword);
        $manager->persist($agency2);
        $this->addReference('MANAGER2', $agency2);
        //</editor-fold>

        //<editor-fold desc="Categories">
        for ($i = 0; $i < self::CATEGORY; $i++) {
            $category = (new Category())->setLabel($this->faker->name());
            $manager->persist($category);
            $this->addReference('CAT' . $i, $category);
        }
        //</editor-fold>

        //<editor-fold desc="Employers">
        $employer = (new Employer())
            ->setName($this->faker->company())
            ->setWebsite($this->faker->url());
        $manager->persist($employer);
        //</editor-fold>

        //<editor-fold desc="Job ads">
        for ($i = 0; $i < self::JOBADS; $i++) {
            /** @var User $agency */
            $agency  = $this->getReference('MANAGER'.\rand(1, 2));
            /** @var Category $category */
            $category = $this->getReference('CAT'.\rand(0, self::CATEGORY - 1));
            $jobAd = (new JobAd())
                ->setTitle($this->faker->sentence())
                ->setCategory($category)
                ->setPublished($this->faker->boolean())
                ->setPublishDate($this->faker->dateTimeBetween('-10 days', '+20 days'))
                ->setApplicationDeadline($this->faker->dateTimeBetween('+10 days', '+20 days'))
                ->setMaxExperience(\rand(0, 4))
                ->setMinExperience(\rand(0, 4))
                ->setManager($agency)
                ->setMaxSalary($this->faker->randomFloat(6, 2))
                ->setMinSalary($this->faker->randomFloat(6, 2))
                ->setEmployer($employer)
                ->setEmploymentStatus('Full-time')
                ->setDescription(\sprintf(
                    '<p>%s</p>',
                    \implode('</p><p>', $this->faker->paragraphs())
                ));
            $manager->persist($jobAd);
        }
        //</editor-fold>
        $manager->flush();
    }
}
