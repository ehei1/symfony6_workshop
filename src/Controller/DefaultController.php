<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Employer;
use App\Entity\JobAd;
use App\Entity\User;
use App\Form\Type\ChangePasswordType;
use App\Form\Type\EmployerType;
use App\Form\Type\ProfileType;
use App\Form\Type\SearchJobAdType;
use App\Model\ChangePassword;
use App\Model\Search;
use App\Repository\JobAdRepository;
use App\Security\Voter\JobAdVoter;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_default')]
    public function index(JobAdRepository $repository, Request $request): Response
    {
        $jobAds = $repository->findBy(['published' => true], ['id' => 'DESC'], 6);
        $recentJobs = \array_chunk($jobAds, 2);
        $initialData = new Search();
        $form = $this->createForm(SearchJobAdType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // process
            $newValues = $form->getData();
            dump($initialData);
            dump($newValues);
            die;
        }

        /*return $this->renderForm('default/index.html.twig', [
            'recentJobs' => $recentJobs,
            'searchForm' => $form
        ]);*/
        return $this->render('default/index.html.twig', [
            'recentJobs' => $recentJobs,
            'searchForm' => $form->createView()
        ]);
    }

    #[Route(path: '/about-us', name: 'about_us')]
    public function aboutUs(): Response
    {
        return $this->render('default/about_us.html.twig');
    }

    #[Route(path: '/employer/add', name: 'add_employer')]
    public function addEmployer(Request $request): Response
    {
        $employer = new Employer();
        $form = $this->createForm(EmployerType::class, $employer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        }

        return $this->render(view: 'employer/add_employer.html.twig', parameters: [
            'employerForm' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route(path: "/profile", name: "app_profile")]
    public function profile(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
            $this->addFlash('success', 'Your profile has been updated');

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('default/profile.html.twig', [
            'profileForm' => $form->createView(),
        ]);
    }

    #[Route(path: "/change-password", name: "app_change_password")]
    public function changePassword(
        Request $request,
        UserPasswordHasherInterface $hasher,
        EntityManagerInterface $manager
    ): Response
    {
        $changePassword = new ChangePassword();
        $form = $this->createForm(ChangePasswordType::class, $changePassword);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // ... persist to database
            /** @var User $user */
            $user = $this->getUser();
            $hashedPassword = $hasher->hashPassword($user, $changePassword->getNewPassword());
            $user->setPassword($hashedPassword);

            $manager->flush();

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('default/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    //     * @Security("is_granted('ROLE_RECRUITMENT_AGENCY') and user == ad.getManager() and ad.isPublished() == true")

    /**
     * This controller should show only visible job ads for public.
     */
    #[Route("/job-details/{slug}", name: "app_view_job")]
    public function viewJobPage(JobAd $ad): Response
    {
        $this->denyAccessUnlessGranted(JobAdVoter::CAN_VIEW, $ad);

        return $this->render('default/job_detail.html.twig', ['job' => $ad]);

//        if ($ad->isPublished() === true &&
//            $this->getUser() === $ad->getManager() &&
//            $this->isGranted('ROLE_RECRUITMENT_AGENCY')) {
//        }
//
//        $this->createAccessDeniedException();
    }
}
