<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\JobAd;
use App\Event\JobAdApplyEvent;
use App\Repository\JobAdRepository;
use App\Security\Voter\JobAdVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Security("is_granted('ROLE_USER')")]
#[Route(path: "/job", name: "app_job_")]
class JobController extends AbstractController
{
    public function __construct(private TokenStorageInterface $tokenStorage, private JobAdRepository $jobAdRepository)
    {
    }

    #[Security("is_granted('ROLE_RECRUITMENT_AGENCY')")]
    #[Route('/manage', name: 'manage')]
    public function manage(): Response
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $jobs = $this->jobAdRepository->findBy(['manager' => $user]);

        return $this->render('job/index.html.twig', [
            'jobs' => $jobs,
        ]);
    }

    #[Route(path: '/edit/ad/{id}', name: 'edit_ad')]
    public function editJobAd(JobAd $jobAd): Response
    {
        $this->denyAccessUnlessGranted(JobAdVoter::CAN_EDIT, $jobAd);

        return new Response('I can edit this job ad');
    }

    #[Route(path: "/apply/{slug}", name: "apply")]
    public function apply(JobAd $offer, EventDispatcherInterface $dispatcher): Response
    {
        $event = new JobAdApplyEvent($offer);
        $dispatcher->dispatch($event, $event::NAME);

        return new Response('Application successfully registered');
    }
}
