<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Education;
use App\Entity\Resume;
use App\Form\Type\ResumeType;
use App\Repository\ResumeRepository;
use App\Service\ResumeManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ResumeController extends AbstractController
{
    public function __construct(private ResumeManager $resumeManager)
    {
    }

    #[Route('/resume', name: 'app_resume')]
    #[Route('/resume/{id}', name: 'app_edit_resume')]
    public function index(Request $request, ?Resume $resume = null): Response
    {
        if (null === $resume) {
            $resume = new Resume();
        }
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $resume->setUpdatedAt(new \DateTime());
            $this->resumeManager->saveResume($resume);

            return $this->redirectToRoute(route: 'app_profile');
        }

        return $this->render('resume/index.html.twig', [
            'resumeForm' => $form->createView(),
        ]);
    }

//    #[Route(path: "/profile", name: "app_profile")]
//    public function profile(): Response
//    {
//        $this->resumeManager->attachResumeToUser();
//
//        return $this->render('resume/profile.html.twig');
//    }
}
