<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\JobAd;

/**
 * This event is triggered whenever a user applies to a job offer.
 */
class JobAdApplyEvent
{
    public const NAME = 'job.apply';

    public function __construct(private JobAd $ad)
    {
    }

    public function getAd(): JobAd
    {
        return $this->ad;
    }
}
