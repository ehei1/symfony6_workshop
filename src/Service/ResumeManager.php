<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Education;
use App\Entity\Resume;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class ResumeManager
{
    private SessionInterface $session;

    public function __construct(
        private EntityManagerInterface $manager,
        RequestStack $requestStack,
        private Security $security
    ) {
        $this->session = $requestStack->getSession();
    }

    /**
     * Persists the given resume  to the database.
     *
     * @param Resume $resume
     */
    public function saveResume(Resume $resume): void
    {
        if (null === $resume->getId()) {
            $this->manager->persist($resume);
        }
        /** @var Education $education */
        foreach ($resume->getEducations() as $education) {
            $education->setResume($resume);
            $this->manager->persist($education);
        }
        $this->manager->flush();
        $this->storeResumeInTheSession($resume);
    }

    /**
     * Attach the current use to the resume.
     */
    public function attachResumeToUser(): void
    {
        if ($this->session->has('resume_id') === true) {
            $resume = $this->getResumeById($this->session->get('resume_id'));
            $this->doAttachUser($resume);
            $this->removeResumeFromSession();
        }
    }

    /**
     * Gets the newly created Resume by its id.
     *
     * @param int $id
     *
     * @return Resume
     */
    public function getResumeById($id): Resume
    {
        return $this->manager
            ->getRepository(Resume::class)
            ->find($id);
    }

    /**
     * Removes the newly created resume id from the session.
     */
    private function removeResumeFromSession(): void
    {
        $this->session->remove('resume_id');
    }

    /**
     * Attach the current user to the newly created Resume.
     *
     * @param Resume $resume
     */
    private function doAttachUser(Resume $resume): void
    {
        // get the connected user.
        /** @var User $user */
        $user = $this->security->getUser();
        $resume->setOwner($user);
        $this->manager->flush();
    }

    /**
     * Stores the id of the Resume in the session.
     *
     * @param Resume $resume
     */
    private function storeResumeInTheSession(Resume $resume): void
    {
        $this->session->set('resume_id', $resume->getId());
    }
}
