<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\JobAd;
use App\Repository\JobAdRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class OldOffersCleanCommand extends Command
{
    private const NB_DAYS = 15;

    protected static $defaultName = 'app:clean:old-offers';
    protected static $defaultDescription = 'Clean job offers which exceed 30 days.';

    private JobAdRepository $jobAdRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(JobAdRepository $jobAdRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->jobAdRepository = $jobAdRepository;
        $this->entityManager = $entityManager;
    }

    public function configure(): void
    {
        $this
            ->addOption(
                'days',
                'd',
                InputOption::VALUE_REQUIRED,
                'Number of days  to consider offers as old',
                self::NB_DAYS
            )
//            ->addArgument(
//                'number-of-days',
//                InputArgument::OPTIONAL,
//                'Number of days  to consider offers as old',
//                15
//            )
        ;
//        $this->setDescription('Clean job offers which exceed 30 days.');
//        $this->setName('app:clean:old-offers');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
//        $question = new ConfirmationQuestion('Are you sure to continue ?', false);
//        if ($helper->ask($input, $output, $question) === false) {
//            return self::SUCCESS;
//        }

//        $nameQuestion = new Question('Enter your name : ');
//        $name = $helper->ask($input, $output, $nameQuestion);
//        $output->writeln('Hello <info>'.$name.'</info>');

//        $choice = new ChoiceQuestion(
//            'Which Color do you prefer ?',
//            ['Yellow', 'Black', 'Red'],
//            1
//        );
//        $choice->setMultiselect(true);
//
//        $preferredColor = $helper->ask($input, $output, $choice);
//        dump($preferredColor);

//        $bundles = ['AcmeDemoBundle', 'AcmeBlogBundle', 'AcmeStoreBundle'];
//        $question = new Question('Please enter the name of a bundle', 'FooBundle');
//        $question->setAutocompleterValues($bundles);
//
//        $bundleName = $helper->ask($input, $output, $question);

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }



        $section1 = $output->section();

        $progressBar1 = new ProgressBar($section1, 50);
        for ($i = 1; $i <= 50; $i++) {
            $progressBar1->advance();
            sleep(1);
        }
        $progressBar1->finish();


        $section2 = $output->section();
        $progressBar2 = new ProgressBar($section2, 50);
        for ($i = 1; $i <= 50; $i++) {
            $progressBar2->advance();
            sleep(10);
        }
        $progressBar2->finish();






//        $output->writeln('Ok you prefer '.\implode(', ', $preferredColor));

        return self::SUCCESS;

//        $numberOfDays = $input->getArgument('number-of-days');
        $numberOfDays = (int)$input->getOption('days');
        $io = new SymfonyStyle($input, $output);
        $io->title('Old Offers Cleaner');
        $io->block('This command will remove all offers published earlier than ' . $numberOfDays . ' days');
        $headers = ['ID', 'TITLE', 'PUBLISH DATE', 'DEADLINE'];

        /** @var ?JobAd[] $offers */
        $offers = $this->jobAdRepository->getPublishedJobOffersOlderThan($numberOfDays);
        if (empty($offers) === true) {
            $io->error('Cannot update offers because the list was empty.');

            return self::FAILURE;
        }

        $rows = $this->buildTableRows($offers);

        $io->table($headers, $rows);
        $this->entityManager->flush();
        $io->success(\sprintf('%s offers have been updated', \count($offers)));
//        $output->writeln('This is a <info>simple</info> message');
//        $output->writeln('This is a <error>simple</error> message');
//        $output->writeln('This is a <comment>simple</comment> message');

        return self::SUCCESS;
    }

    /**
     * @param JobAd[] $offers
     *
     * @return array<int, <int, string>>
     */
    private function buildTableRows(array $offers): array
    {
        $rows = [];
        foreach ($offers as $offer) {
            $rows[] = [
                $offer->getId(),
                $offer->getTitle(),
                $offer->getPublishDate()->format('d/m/Y'),
                $offer->getApplicationDeadline()->format('d/m/Y')
            ];
            $offer->setPublished(false);
        }

        return $rows;
    }
}
