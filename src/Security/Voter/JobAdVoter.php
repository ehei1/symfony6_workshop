<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\JobAd;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class JobAdVoter extends Voter
{
    const CAN_EDIT = 'CAN_EDIT_JOB_AD';
    const CAN_VIEW = 'CAN_VIEW_JOB_AD';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return (self::CAN_EDIT === $attribute || self::CAN_VIEW === $attribute)
            && $subject instanceof JobAd;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var JobAd $jobAd */
        $jobAd = $subject;

        return match ($attribute) {
            self::CAN_EDIT => $this->isOwner($jobAd, $token),
            self::CAN_VIEW => $this->isAbleToView($jobAd, $token)
        };
    }

    /**
     * Checks if the current user view the given job ad.
     *
     * @param JobAd          $jobAd The job ad instance
     * @param TokenInterface $token The token interface instance
     *
     * @return bool True if the current user can view the job ad, false otherwise
     */
    private function isAbleToView(JobAd $jobAd, TokenInterface $token): bool
    {
        return $this->isOwner($jobAd, $token) || ($jobAd->isPublished() === true);
    }

    /**
     * Checks if the current user is actually the job ad's owner.
     *
     * @param JobAd          $jobAd The job ad instance
     * @param TokenInterface $token The token interface instance
     *
     * @return bool True if the current user is the owner, false otherwise
     */
    private function isOwner(JobAd $jobAd, TokenInterface $token): bool
    {
        return $jobAd->getManager() === $token->getUser();
    }
}
