<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ErrorNotFoundListener
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    public function onNotFoundError(ExceptionEvent $event): void
    {
        /** @var NotFoundHttpException $exception */
        $exception = $event->getThrowable();

        if (Response::HTTP_NOT_FOUND === $exception->getStatusCode()) {
            // we need to send a notification email to the IT Support
            $email = (new Email())
                ->from('example@symfony.com')
                ->to('it-support@job.com')
                ->subject('Page not found')
                ->text('User tried to acces to '.$event->getRequest()->getBaseUrl());
            $this->mailer->send($email);
        }
    }
}
