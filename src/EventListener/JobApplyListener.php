<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\JobAdApplyEvent;

class JobApplyListener
{
    /**
     * Event dispatched whenever an appliance to an offer has been set.
     *
     * @param JobAdApplyEvent $event
     */
    public function onJobApply(JobAdApplyEvent $event): void
    {
    }
}
