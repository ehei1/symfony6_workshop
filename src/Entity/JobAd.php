<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\JobAdRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Slug;

#[ORM\Entity(repositoryClass: JobAdRepository::class)]
class JobAd
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'date')]
    private $publishDate;

    #[ORM\Column(type: 'string', length: 255)]
    private $employmentStatus;

    #[ORM\Column(type: 'integer')]
    private $minExperience;

    #[ORM\Column(type: 'integer')]
    private $maxExperience;

    #[ORM\Column(type: 'date', nullable: true)]
    private $applicationDeadline;

    #[ORM\Column(type: 'float')]
    private $minSalary;

    #[ORM\Column(type: 'float')]
    private $maxSalary;

    #[ORM\Column(type: 'boolean')]
    private $published;

    #[ORM\Column(type: 'string', length: 255)]
    #[Slug(fields: ['title'])]
    private $slug;

    #[ORM\ManyToOne(targetEntity: Employer::class, inversedBy: 'jobAds')]
    #[ORM\JoinColumn(nullable: false)]
    private $employer;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'jobAds')]
    #[ORM\JoinColumn(nullable: false)]
    private $category;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'jobAds')]
    #[ORM\JoinColumn(nullable: false)]
    private $manager;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTimeInterface $publishDate): self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    public function getEmploymentStatus(): ?string
    {
        return $this->employmentStatus;
    }

    public function setEmploymentStatus(string $employmentStatus): self
    {
        $this->employmentStatus = $employmentStatus;

        return $this;
    }

    public function getMinExperience(): ?int
    {
        return $this->minExperience;
    }

    public function setMinExperience(int $minExperience): self
    {
        $this->minExperience = $minExperience;

        return $this;
    }

    public function getMaxExperience(): ?int
    {
        return $this->maxExperience;
    }

    public function setMaxExperience(int $maxExperience): self
    {
        $this->maxExperience = $maxExperience;

        return $this;
    }

    public function getApplicationDeadline(): ?\DateTimeInterface
    {
        return $this->applicationDeadline;
    }

    public function setApplicationDeadline(?\DateTimeInterface $applicationDeadline): self
    {
        $this->applicationDeadline = $applicationDeadline;

        return $this;
    }

    public function getMinSalary(): ?float
    {
        return $this->minSalary;
    }

    public function setMinSalary(float $minSalary): self
    {
        $this->minSalary = $minSalary;

        return $this;
    }

    public function getMaxSalary(): ?float
    {
        return $this->maxSalary;
    }

    public function setMaxSalary(float $maxSalary): self
    {
        $this->maxSalary = $maxSalary;

        return $this;
    }

    public function isPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEmployer(): ?Employer
    {
        return $this->employer;
    }

    public function setEmployer(?Employer $employer): self
    {
        $this->employer = $employer;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }
}
