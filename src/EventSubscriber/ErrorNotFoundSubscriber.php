<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ErrorNotFoundSubscriber implements EventSubscriberInterface
{
    private const ERROR_TAG = '__ERROR_404__';

    public function __construct(private LoggerInterface $logger)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onNotFoundError', 2],
                ['preNotFoundError', 3],
                ['posNotFoundError', 1]
            ],
        ];
    }

    public function onNotFoundError(ExceptionEvent $event): void
    {
        $this->logger->error(self::ERROR_TAG, ['message' => __METHOD__]);
    }

    public function preNotFoundError(ExceptionEvent $event): void
    {
        $this->logger->error(self::ERROR_TAG, ['message' => __METHOD__]);
    }

    public function posNotFoundError(ExceptionEvent $event): void
    {
        $this->logger->error(self::ERROR_TAG, ['message' => __METHOD__]);
    }
}
