<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Employer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class EmployerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employer::class);
    }

    public function checkEmployerUnicity(array $criteria)
    {
        return $this->createQueryBuilder('e')
            ->where('e.name = :n')
            ->andWhere('e.website = :s')
            ->setParameter('n', $criteria['name'])
            ->setParameter('s', $criteria['website'])
            ->getQuery()
            ->getResult();
    }
}
