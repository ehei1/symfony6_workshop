<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\JobAd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

class JobAdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobAd::class);
    }

    /**
     * Retrieve all published offers that have been posted $days ago.
     */
    public function getPublishedJobOffersOlderThan(int $days)
    {
        $queryBuilder = $this->createQueryBuilder('job');
        $limitDate = (new \DateTime())->sub(new \DateInterval('P'.$days.'D'));

        return $queryBuilder
            ->where('job.published = 1')
            ->andWhere('job.publishDate < :limit')
            ->andWhere('job.applicationDeadline < :today')
            ->setParameter('limit', $limitDate)
            ->setParameter('today', new \DateTime())
            ->orderBy('job.publishDate', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
