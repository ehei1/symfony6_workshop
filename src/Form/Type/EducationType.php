<?php


namespace App\Form\Type;


use App\Entity\Education;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('degree', TextType::class, [
                'attr' => [
                    'placeholder' => 'Degree, e.g. Bachelor',
                ]
            ])
            ->add('fieldOfStudy', TextType::class, [
                'attr' => [
                    'placeholder' => 'Major, e.g Computer Science',
                ],
            ])
            ->add('fromYear', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g 2014',
                ],
            ])
            ->add('toYear', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'e.g 2020',
                ],
            ])
            ->add('school', TextType::class, [
                'attr' => [
                    'placeholder' => 'School name, e.g. Massachusetts Institute of Technology',
                ],
            ])
            ->add('description', TextareaType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
            'label' => false,
        ]);
    }
}