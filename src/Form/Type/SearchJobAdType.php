<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\Search;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchJobAdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('where', TextType::class, [
//                'block_prefix' => 'where',
                'block_name' => 'location',
                'label' => 'Where ?',
                'attr' => [
                   'placeholder' => 'Where do you want to search for ?',
                 ],
                'help' => 'Type here where do you want to find an Advert',
             ])
            ->add('what', TextType::class, [
                'attr' => [
                    'placeholder' => 'What are you looking for...',
                ],
            ])
            ->add('category', ChoiceType::class, [
                'label' => 'Category',
                'placeholder' => 'All categories',
                'choices' => [
                    'Finances' => 'FIN',
                    'Software Engineering' => 'SOFTEN',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Search::class,
            'label' => false,
        ]);
    }
}
