<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\BasicInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BasicInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('fullName', TextType::class)
            ->add('email', EmailType::class)
            ->add('website', UrlType::class, [
                'required' => false,
            ])
            ->add('age', IntegerType::class)
            ->add('location', TextType::class)
            ->add('professionTitle', TextType::class, [
//                'constraints' => [
//                    new Assert\Length(['min' => 10, 'minMessage' => 'ce champs doit etre  de min 10 caractres']),
//                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BasicInformation::class,
            'label' => false,
        ]);
    }

    /**
     * This method is exactly the same as 'block_prefix' => 'basic' in the ResumeType::class -> basicInformation field.
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'basic';
    }
}
