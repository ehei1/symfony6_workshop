# symfony6_workshop

Symfony 6 project - Workshop

# Commands to run before launching the app

composer install

php  bin/console doctrine:migrations:migrate

php bin/console doctrine:fixtures:load

