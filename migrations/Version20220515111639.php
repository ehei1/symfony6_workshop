<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220515111639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_ad ADD manager_id INT NOT NULL');
        $this->addSql('ALTER TABLE job_ad ADD CONSTRAINT FK_C339C4A1783E3463 FOREIGN KEY (manager_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C339C4A1783E3463 ON job_ad (manager_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_ad DROP FOREIGN KEY FK_C339C4A1783E3463');
        $this->addSql('DROP INDEX IDX_C339C4A1783E3463 ON job_ad');
        $this->addSql('ALTER TABLE job_ad DROP manager_id');
    }
}
