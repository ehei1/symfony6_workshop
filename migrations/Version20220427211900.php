<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427211900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE basic_information (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, location VARCHAR(255) NOT NULL, profession_title VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE education (id INT AUTO_INCREMENT NOT NULL, resume_id INT NOT NULL, degree VARCHAR(255) NOT NULL, field_of_study VARCHAR(255) NOT NULL, from_year INT NOT NULL, to_year INT NOT NULL, school VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_DB0A5ED2D262AF09 (resume_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_ad (id INT AUTO_INCREMENT NOT NULL, employer_id INT NOT NULL, category_id INT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, publish_date DATE NOT NULL, employment_status VARCHAR(255) NOT NULL, min_experience INT NOT NULL, max_experience INT NOT NULL, application_deadline DATE DEFAULT NULL, min_salary DOUBLE PRECISION NOT NULL, max_salary DOUBLE PRECISION NOT NULL, published TINYINT(1) NOT NULL, slug VARCHAR(255) NOT NULL, INDEX IDX_C339C4A141CD9E7A (employer_id), INDEX IDX_C339C4A112469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resume (id INT AUTO_INCREMENT NOT NULL, basic_information_id INT NOT NULL, owner_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_60C1D0A0521FCB9D (basic_information_id), INDEX IDX_60C1D0A07E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, full_name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE education ADD CONSTRAINT FK_DB0A5ED2D262AF09 FOREIGN KEY (resume_id) REFERENCES resume (id)');
        $this->addSql('ALTER TABLE job_ad ADD CONSTRAINT FK_C339C4A141CD9E7A FOREIGN KEY (employer_id) REFERENCES employer (id)');
        $this->addSql('ALTER TABLE job_ad ADD CONSTRAINT FK_C339C4A112469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE resume ADD CONSTRAINT FK_60C1D0A0521FCB9D FOREIGN KEY (basic_information_id) REFERENCES basic_information (id)');
        $this->addSql('ALTER TABLE resume ADD CONSTRAINT FK_60C1D0A07E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE resume DROP FOREIGN KEY FK_60C1D0A0521FCB9D');
        $this->addSql('ALTER TABLE job_ad DROP FOREIGN KEY FK_C339C4A112469DE2');
        $this->addSql('ALTER TABLE job_ad DROP FOREIGN KEY FK_C339C4A141CD9E7A');
        $this->addSql('ALTER TABLE education DROP FOREIGN KEY FK_DB0A5ED2D262AF09');
        $this->addSql('ALTER TABLE resume DROP FOREIGN KEY FK_60C1D0A07E3C61F9');
        $this->addSql('DROP TABLE basic_information');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE education');
        $this->addSql('DROP TABLE employer');
        $this->addSql('DROP TABLE job_ad');
        $this->addSql('DROP TABLE resume');
        $this->addSql('DROP TABLE user');
    }
}
